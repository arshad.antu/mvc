<?php 

	class Help extends Controller {

		function __construct() {
			parent::__construct();
			echo "We are in Controller HELP.<br>";
		}

		public function math($arg = false) {
			echo "This function does a math.<br>";
			if ($arg && is_numeric($arg)) {
				echo "Number: ".$arg."<br>";
			}
		}
	}

?>