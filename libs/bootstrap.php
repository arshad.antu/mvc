<?php 

	class Bootstrap {

		function __construct() {			
			if (!empty($_GET)) {
				$url = $_GET['url'];
				$url = explode('/', rtrim($url, '/'));

				//print_r($url); //Debugger to check url

				$file = "conts/".$url[0].".php";

				if (file_exists($file)) { 
					require $file; 
				} else {
					require 'conts/error.php';
					$e = new ErrorController();
					return false;
				}

				$cont = new $url[0];

				if (isset($url[2])) {
					$cont->{$url[1]}($url[2]);
				} else if (isset($url[1])) {		 
					$cont->{$url[1]}();		
				}
			} else {
				header('Location: ./index');
				die();
			}			
		}
	}

?>